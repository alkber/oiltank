#include <NewPing.h>
//#include <SPI.h>
//#include <Wire.h>
//#include <LiquidCrystal_I2C.h>          // for LCD w/ GPIO MODIFIED for the ATtiny85
//#include <Adafruit_MCP23017.h>
//#include <Adafruit_RGBLCDShield.h>

//#define GPIO_ADDR     0x27             // (PCA8574A A0-A2 @5V) typ. A0-A3 Gnd 0x20 / 0x38 for A - 0x27 is the address of the Digispark LCD modules.

//Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

#define TEMP_PIN     2
#define LED_PIN      10
#define TRIGGER_PIN  12  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     11  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define HEAD_SPACE   10  // head space of the tank in mm
#define HEIGHT       1219 // height of tank in mm
#define LENGTH       1854 // length of tank in mm
#define TOTAL_VOLUME 572 // volume of tank in gallons

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup() {
  Serial.begin(9600); // Open serial monitor at 9600 baud to see ping results.
//  lcd.begin(16, 2);
//  lcd.setBacklight(0x1);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
}

void loop() {
  // delay should be at least 29ms
  int mm = readDistance();
  Serial.print(mm); // Convert ping time to distance in cm and print result (0 = outside set distance range)
  Serial.print("mm, ");
  // print calculated volume
  /*
  float chord = 2 * sqrt(2 * mm * HEIGHT / 2 - mm * mm); // chord length
  float angle = acos((HEIGHT/2 - mm)/(HEIGHT/2)) * 2;    // angle
  float arc = angle * HEIGHT / 2;                        // arc length
  float area = (arc * HEIGHT / 2 - chord * (HEIGHT / 2 - mm)) / 2; // cross area
  float volume = area / (HEIGHT * HEIGHT / 4 * PI) * TOTAL_VOLUME; //volume
  Serial.print(volume);
  Serial.print(" gal, ");
  */
  // print temperature
  Serial.print((analogRead(TEMP_PIN) * .4882814) * 1.8 + 32);
  Serial.println("F");
//  lcd.setCursor(0, 0);
//  lcd.print(uS / US_ROUNDTRIP_CM);
//  lcd.print("     ");
  delay(30000);
}

#define NUM_READS 100
// Take a number of readings and apply a median filter
unsigned int readDistance() {
  // read multiple values and sort them
  unsigned int sortedValues[NUM_READS];
  for (int i = 0; i < NUM_READS; i++) {
    unsigned int value = sonar.ping();
    int j;
    if (value < sortedValues[0] || i == 0) {
      j = 0;
    }
    else {
      for (j = 1; j < i; j++) {
        if (sortedValues[j-1] <= value && sortedValues[j] >= value) {
          // j is insert position
          break;
        }
      }
    }
    for (int k = i; k > j; k--) {
      // move all values higher than current reading up one position
      sortedValues[k] = sortedValues[k - 1];
    }
    sortedValues[j] = value; // insert current reading
    delay(30);
  }
  Serial.print(10 * sortedValues[0] / US_ROUNDTRIP_CM);
  Serial.print("-");
  Serial.println(10 * sortedValues[NUM_READS - 1] / US_ROUNDTRIP_CM);
  // return scaled mode of 10 values
  float returnval = 0;
  for (int i = NUM_READS/2 - 5; i < (NUM_READS / 2 + 5); i++) {
    returnval += sortedValues[i];
  }
  return returnval / US_ROUNDTRIP_CM;
}
